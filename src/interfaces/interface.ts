import { Ref } from 'react';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';
import { RouteProp } from '@react-navigation/core';
import { SignatureViewRef } from 'react-native-signature-canvas';

/* Navigation */
export type RouteParamList = {
  Home: undefined;
};

export type PropsNavigation<T extends keyof RouteParamList> = {
  navigation: NativeStackNavigationProp<RouteParamList, T>;
  route: RouteProp<RouteParamList, T>;
};

/* Hooks */
// useSignature
export interface ISignature {
  descriptionText: string;
  penColor: string;
}
