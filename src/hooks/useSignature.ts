import { RefObject, useState } from 'react';
import { SignatureViewRef } from 'react-native-signature-canvas';

function useSignature(refSignature: RefObject<SignatureViewRef>) {
  const [signatureImg, setSignatureImg] = useState('');

  const handleImgSignature = (img: string) => {
    setSignatureImg(img);
  };

  const handleConfirm = () => {
    setSignatureImg('');
    refSignature.current?.readSignature();
  };

  const handleClear = () => {
    setSignatureImg('');
    refSignature.current?.clearSignature();
  };

  return {
    signatureImg,
    handleImgSignature,
    handleConfirm,
    handleClear,
  };
}

export default useSignature;
