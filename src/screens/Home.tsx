import React, { useEffect, useRef } from 'react';
import { Box } from 'native-base';
import * as ScreenOrientation from 'expo-screen-orientation';
import SignatureScreen from '../components/SignatureScreen';

export default function Home() {
  const changeOrientation = async () => {
    const screenLandscape = ScreenOrientation.OrientationLock.LANDSCAPE;
    try {
      await ScreenOrientation.lockAsync(screenLandscape);
    } catch (error) {
      console.log(error);
    }
  };

  const defaultOrientation = async () => {
    const screenPortrait = ScreenOrientation.OrientationLock.PORTRAIT;
    try {
      await ScreenOrientation.lockAsync(screenPortrait);
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    changeOrientation();
    return () => {
      defaultOrientation();
    };
  });

  return (
    <Box flex={1} safeArea>
      <SignatureScreen descriptionText="prueba" penColor="#000" />
    </Box>
  );
}
