import React from 'react';
import { NavigationContainer as NavigationBox } from '@react-navigation/native';
import {
  createNativeStackNavigator,
  NativeStackNavigationOptions,
} from '@react-navigation/native-stack';
import 'react-native-gesture-handler';
import { RouteParamList } from '../interfaces/interface';
import Home from './Home';

const Stack = createNativeStackNavigator<RouteParamList>();

function MainStack() {
  const screenOptions: NativeStackNavigationOptions = {
    gestureEnabled: true,
    headerShown: false,
    animation: 'slide_from_right',
  };

  return (
    <NavigationBox>
      <Stack.Navigator initialRouteName="Home" screenOptions={screenOptions}>
        <Stack.Screen name="Home" component={Home} />
      </Stack.Navigator>
    </NavigationBox>
  );
}

export default MainStack;
