import React, { useState, useRef } from 'react';
import { View } from 'react-native';
import { Image, Box, Modal, Button, useToast } from 'native-base';
import Signature, { SignatureViewRef } from 'react-native-signature-canvas';
import { ISignature } from '../interfaces/interface';
import Sign from './SignatureScreen/Sign';
import SignatureTools from './SignatureScreen/SignatureTools';

function SignatureScreen(props: ISignature) {
  const { descriptionText, penColor } = props;
  const refSignature = useRef<SignatureViewRef>(null);
  const [signatureImg, setSignatureImg] = useState('');
  const [openModal, setOpenModal] = useState(false);
  const toast = useToast();
  const hiddenSignatureDefaultButtons = `.m-signature-pad {box-shadow: none; border: none; } .m-signature-pad--body {border: none;} .m-signature-pad--footer {display: none; margin: 0px;}`;

  const saveImg = () => {
    setOpenModal(!openModal);
    toast.show({
      render: () => {
        return (
          <Box bg="emerald.500" px="2" py="1" rounded="sm" mb={5}>
            Firma Guardada con Exito
          </Box>
        );
      },
      placement: 'bottom',
    });
  };

  const handleImgSignature = (img: string) => {
    setSignatureImg(img);
  };

  const handleConfirm = () => {
    setOpenModal(!openModal);
    refSignature.current?.readSignature();
  };

  const handleClear = () => {
    refSignature.current?.clearSignature();
    setSignatureImg('');
  };

  return (
    <Box flex={1} alignItems="center" justifyContent="center" maxH="full">
      <Signature
        ref={refSignature}
        onOK={handleImgSignature}
        descriptionText={descriptionText}
        webStyle={hiddenSignatureDefaultButtons}
        penColor={penColor}
      />
      <Sign handleConfirm={handleConfirm} />
      <SignatureTools handleClear={handleClear} />
      <Modal isOpen={openModal} size="xl">
        <Modal.Content>
          <Modal.Header>Signature Preview</Modal.Header>
          <Modal.Body flex={1}>
            {signatureImg ? (
              <Image
                resizeMode="contain"
                source={{ uri: signatureImg }}
                alt="descriptionText"
                w="full"
                h={200}
              />
            ) : null}
          </Modal.Body>
          <Modal.Footer>
            <Button.Group>
              <Button onPress={() => setOpenModal(!openModal)}>Cancel</Button>
              <Button onPress={saveImg}>Save</Button>
            </Button.Group>
          </Modal.Footer>
        </Modal.Content>
      </Modal>
    </Box>
  );
}

export default SignatureScreen;
