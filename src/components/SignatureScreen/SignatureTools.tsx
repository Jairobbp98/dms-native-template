import React from 'react';
import { Button, useTheme, Icon } from 'native-base';
import { IButtonProps } from 'native-base/lib/typescript/components/primitives/Button';
import { Feather as FeatherIcons } from '@expo/vector-icons';
import Broom from '../Icons/Icons';

interface IButtonTools {
  styleLight: IButtonProps;
  styleDark: IButtonProps;
  Event: () => void;
  childrenIcon: JSX.Element;
}

interface ISignatureTools {
  handleClear: () => void;
}

function ButtonTool(props: IButtonTools) {
  const { styleLight, styleDark, Event, childrenIcon } = props;
  return (
    <Button
      leftIcon={<Icon as={childrenIcon} />}
      _light={styleLight}
      _dark={styleDark}
      onPress={Event}
      width={12}
      height={12}
    />
  );
}

function SignatureTools(props: ISignatureTools) {
  const { handleClear } = props;
  const { colors, size } = useTheme();
  const { dark, light } = colors;
  const styleLight: IButtonProps = {
    bg: light.Primary.primaryContainer,
  };
  const styleDark: IButtonProps = {
    bg: dark.Primary.primaryContainer,
  };

  return (
    <Button.Group position="absolute" direction="column" right={3} top={3}>
      <ButtonTool
        childrenIcon={<Broom size={size.md} />}
        styleLight={styleLight}
        styleDark={styleDark}
        Event={handleClear}
      />
      <ButtonTool
        childrenIcon={<FeatherIcons name="droplet" size={size.md} />}
        styleLight={styleLight}
        styleDark={styleDark}
        Event={handleClear}
      />
      <ButtonTool
        childrenIcon={<FeatherIcons name="underline" size={size.md} />}
        styleLight={styleLight}
        styleDark={styleDark}
        Event={handleClear}
      />
    </Button.Group>
  );
}

export default SignatureTools;
