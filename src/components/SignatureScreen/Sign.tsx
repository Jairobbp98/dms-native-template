import React from 'react';
import { Fab, useTheme, Icon } from 'native-base';
import { Feather as FeatherIcon } from '@expo/vector-icons';

interface ISign {
  handleConfirm: () => void;
}

export default function Sign(props: ISign) {
  const { handleConfirm } = props;
  const { colors } = useTheme();
  const { dark, light } = colors;

  return (
    <Fab
      icon={
        <Icon
          as={FeatherIcon}
          name="edit-3"
          size="sm"
          _light={{
            color: light.Primary.primary,
          }}
          _dark={{
            color: dark.Primary.primary,
          }}
        />
      }
      onPress={handleConfirm}
      _light={{
        bg: light.Primary.primaryContainer,
        color: light.Primary.onPrimary,
      }}
      _dark={{
        bg: dark.Primary.primaryContainer,
        color: dark.Primary.onPrimary,
      }}
      label="Firmar"
    />
  );
}
