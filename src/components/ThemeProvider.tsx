import React from 'react';
import { NativeBaseProvider } from 'native-base';
import Theme from '../constants/Theme';
import ThemeMode from '../constants/ThemeMode';

function ThemeProvider({ children }: { children: JSX.Element }) {
  return (
    <NativeBaseProvider theme={Theme} colorModeManager={ThemeMode}>
      {children}
    </NativeBaseProvider>
  );
}

export default ThemeProvider;
