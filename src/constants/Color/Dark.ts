const Dark = {
  Primary: {
    primary: '#a4c8ff',
    onPrimary: '#003061',
    primaryContainer: '#004789',
    onPrimaryContainer: '#d3e3ff',
  },
  Secondary: {
    secondary: '#bdc7dc',
    onSecondary: '#273141',
    secondaryContainer: '#3D4758',
    onSecondaryContainer: '#d8e3f8',
  },
  Tertiary: {
    tertiary: '#DABCE2',
    onTertiary: '#3E2846',
    tertiaryContainer: '#553F5D',
    onTertiaryContainer: '#F7D8FE',
  },
  Error: {
    error: '#ffb4a9',
    onError: '#680003',
    errorContainer: '#930006',
    onErrorContainer: '#FFDAD4',
  },
  Background: {
    background: '#1B1B1D',
    onBackground: '#E3E2E6',
    surface: '#1B1B1D',
    onSurface: '#E3E2E6',
  },
  Surface: {
    variant: '#43474E',
    onVariant: '#C3C6CF',
    outline: '#8E9199',
  },
};

export default Dark;
