const Light = {
  Primary: {
    primary: '#005fb1',
    onPrimary: '#FFFFFF',
    primaryContainer: '#d3e3ff',
    onPrimaryContainer: '#001b3c',
  },
  Secondary: {
    secondary: '#555F71',
    onSecondary: '#FFFFFF',
    secondaryContainer: '#d8e3f8',
    onSecondaryContainer: '#121c2b',
  },
  Tertiary: {
    tertiary: '#6e5676',
    onTertiary: '#FFFFFF',
    tertiaryContainer: '#f7d8fe',
    onTertiaryContainer: '#27142f',
  },
  Error: {
    error: '#ba1b1b',
    onError: '#FFFFFF',
    errorContainer: '#FFDAD4',
    onErrorContainer: '#410001',
  },
  Background: {
    background: '#fdfbff',
    onBackground: '#1b1b1d',
    surface: '#fdfbff',
    onSurface: '#1b1b1d',
  },
  Surface: {
    variant: '#E0E2EB',
    onVariant: '#43474E',
    outline: '#74777F',
  },
};

export default Light;
