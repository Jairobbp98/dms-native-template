/* eslint-disable @typescript-eslint/no-empty-interface */
import { extendTheme } from 'native-base';
import Dark from './Color/Dark';
import Light from './Color/Light';
import StylesComponents from './StylesComponents/StylesComponents';

const customTheme = extendTheme({
  colors: {
    light: Light,
    dark: Dark,
  },
  fontSizes: {
    sm: '12px',
    md: '16px',
    lg: '32px',
    xl: '64px',
  },
  fontConfig: {
    Roboto: {
      100: {
        normal: 'Roboto_100Thin',
        italic: 'Roboto_100Thin_Italic',
      },
      300: {
        normal: 'Roboto_300Light',
        italic: 'Roboto_300Light_Italic',
      },
      400: {
        normal: 'Roboto_400Regular',
        italic: 'Roboto_400Regular_Italic',
      },
      500: {
        normal: 'Roboto_500Medium',
        italic: 'Roboto_500Medium_Italic',
      },
      700: {
        normal: 'Roboto_700Bold',
      },
    },
  },
  fonts: {
    heading: 'Roboto',
    title: 'Roboto',
    subtitle: 'Roboto',
    text: 'Roboto',
    FAB: 'Roboto',
    Button: 'Roboto',
  },
  space: {
    s: 8,
    m: 16,
    l: 24,
    xl: 40,
  },
  size: {
    sm: 16,
    md: 24,
    lg: 48,
    xl: 96,
  },
  components: StylesComponents,
  config: {
    useSystemColorMode: true,
    initialColorMode: 'light',
  },
});

type CustomThemeType = typeof customTheme;

declare module 'native-base' {
  interface ICustomTheme extends CustomThemeType {}
}

export default customTheme;
