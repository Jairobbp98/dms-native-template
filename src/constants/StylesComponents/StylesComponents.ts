const StylesComponents = {
  FAB: {
    baseStyle: {
      rounded: 16,
    },
  },
  Button: {
    baseStyle: {
      rounded: 16,
    },
  },
};

export default StylesComponents;
