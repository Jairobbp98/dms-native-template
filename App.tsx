import React from 'react';
import ThemeProvider from './src/components/ThemeProvider';
import MainStack from './src/screens/MainStack';

function App() {
  return (
    <ThemeProvider>
      <MainStack />
    </ThemeProvider>
  );
}

export default App;
